﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1_Angeles
{
    class Program
    {

        static void Main(string[] args)
        {
            int playerGold = 1000;
            int playerBet = 0;
            List<int> playerRolls = new List<int>();
            List<int> aiRolls = new List<int>();
            Random random = new Random();

            PlayRound(ref playerGold,ref playerBet,ref playerRolls,ref aiRolls,random);
            Console.WriteLine("You ran out of money!");
            System.Environment.Exit(1);
        }

        static void Stats(ref int money)
        {
            Console.WriteLine("Player\nGold : " + money);
        }

        static void Betting(ref int gold, ref int betInput)
        {
            bool answered = false;
            while (answered == false)
            {
                Console.WriteLine("Please put an amount that you would like to bet : ");
                betInput = Int32.Parse(Console.ReadLine());
                if (betInput <= 0)
                {
                    Console.WriteLine("You must have a bet larger than 0!");
                }
                else if (betInput > gold)
                {
                    Console.WriteLine("You do not have enough to bet this much!");
                }
                else
                {
                    gold -= betInput;
                    Console.WriteLine("You bet " + betInput + " Gold.");
                    answered = true;
                }
            }
            answered = false;
        }

        static void DiceRoll(List<int> dice, Random rand)
        {
            
            int number;
            for (int i = 0; i < 2; i++)
            {
                do
                {
                    number = rand.Next(1, 6);
                } while (dice.Contains(number));
                dice.Add(number);
            }
        }
        
        static void Payout(ref int playerMoney, ref int bet, ref List<int> playerDice, ref List<int> aiDice)
        {
            int playerValue = playerDice[0] + playerDice[1];
            int aiValue = aiDice[0] + aiDice[1];
            if(playerValue == aiValue)
            {
                Console.WriteLine("DRAW! No gains!");
                playerMoney += bet;
            }
            else if(playerValue == 2)
            {
                Console.WriteLine("SNAKE EYES! The player gets a bonus to their payout!\nYou gain " + (bet * 2) + " Gold!");
                playerMoney += (bet * 3);
            }
            else if(aiValue ==2)
            {
                Console.WriteLine("ENEMY SNAKE EYES! Dont worry you'll only lose your bet amount.");
            }
            else if (playerValue > aiValue)
            {
                Console.WriteLine("YOU WIN!\nYou gain " + bet + " Gold!");
                playerMoney += (bet * 2);
            }
            else
            {
                Console.WriteLine("YOU LOST!");
            }
            playerDice.Clear();
            aiDice.Clear();
        }

        static void PlayRound(ref int playerMoney, ref int pBet, ref List<int> pRolls, ref List<int> aRolls,Random rand)
        {
           
            while (playerMoney > 0)
            {
                Stats(ref playerMoney);
                Console.WriteLine("=====================");
                Betting(ref playerMoney,ref pBet);
                Console.WriteLine("=====================");
                DiceRoll(pRolls,rand);
                Console.WriteLine("You have rolled " + pRolls[0] + " & " + pRolls[1]);
                Console.WriteLine("\n");
                DiceRoll(aRolls,rand);
                Console.WriteLine("The AI have rolled " + aRolls[0] + " & " + aRolls[1]);
                Console.WriteLine("=====================");
                Payout(ref playerMoney,ref pBet, ref pRolls, ref aRolls);
                Console.WriteLine("\n");

                Console.ReadLine();
            }
        }
    }
}
