﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2_Angeles
{
    class Shop
    {

        public void shop(ref Player player, Random random)
        {
            bool willExit = false;
            Console.WriteLine("Welcome to the Shop!");
            while (willExit == false)
            {
                Console.WriteLine("\n====================================\n");
                Console.WriteLine("What would you like to do? : ");
                Console.WriteLine("1.) Buy Weapons\n2.) Buy Armor\n3.) Leave Shop");
                int answer = Int32.Parse(Console.ReadLine());
                if (answer == 1)
                {
                    bool answered = false;
                    while (answered == false)
                    {
                        Console.WriteLine("\n====================================\n");
                        Console.WriteLine("What weapon would you like to buy?? : ");
                        Item weapon = new Item();
                        Console.WriteLine("1.) Shortsword\n2.) Longsword\n3.) Broad Sword\n4.) Excalibur\n5.) Back");
                        int response = Int32.Parse(Console.ReadLine());
                        switch (response)
                        {
                            case 1:
                                weapon = new Item("Shortsword", 5, 10);
                                answered = true;
                                break;
                            case 2:
                                weapon = new Item("Longsword", 10, 50);
                                answered = true;
                                break;
                            case 3:
                                weapon = new Item("Broad Sword", 20, 200);
                                answered = true;
                                break;
                            case 4:
                                weapon = new Item("Excalibur", 999, 9999);
                                answered = true;
                                break;
                            case 5:
                                shop(ref player, random);
                                break;
                            default:
                                Console.WriteLine("Invalid Answer!");
                                Console.ReadLine();
                                break;
                        }
                        if (player.hasWeapon == true)
                        {
                            Console.WriteLine("You have a weapon currently equipped! Buying this weapon will replace the weapon you currently have.\nProceed? (Y/N):");
                            string reply = Console.ReadLine();
                            if (reply == "Y" || reply == "y")
                            {
                                if (player.getGold < weapon.Cost)
                                {
                                    Console.WriteLine("It seems you do not have enough gold to purchase this item! Earn more and try again.");
                                    answered = false;
                                }
                                else
                                {
                                    player.WeaponEquipped = weapon;
                                    player.spendMoney(weapon);
                                }

                            }
                            else if (reply == "N" || reply == "n")
                            {
                                answered = false;
                            }
                            else
                            {
                                Console.WriteLine("Invalid Answer!");
                                Console.ReadLine();
                            }
                        }
                        else
                        {
                            Console.WriteLine("Proceed? (Y/N):");
                            string reply = Console.ReadLine();
                            if (reply == "Y" || reply == "y")
                            {
                                if (player.getGold < weapon.Cost)
                                {
                                    Console.WriteLine("It seems you do not have enough gold to purchase this item! Earn more and try again.");
                                    answered = false;
                                }
                                else
                                {
                                    player.hasWeapon = true;
                                    player.WeaponEquipped = weapon;
                                    player.spendMoney(weapon);
                                }
                            }
                            else if (reply == "N" || reply == "n")
                            {
                                answered = false;
                            }
                            else
                            {
                                Console.WriteLine("Invalid Answer!");
                                Console.ReadLine();
                            }
                        }
                    }

                }
                else if (answer == 2)
                {
                    bool answered = false;
                    while (answered == false)
                    {
                        Console.WriteLine("\n====================================\n");
                        Console.WriteLine("What armor would you like to buy?? : ");
                        Item armor = new Item();
                        Console.WriteLine("1.) Leather Mail\n2.) Chain Mail\n3.) Plate Armor\n4.) Back");
                        int response = Int32.Parse(Console.ReadLine());
                        switch (response)
                        {
                            case 1:
                                armor = new Item("Leather Mail", 2, 50);
                                answered = true;
                                break;
                            case 2:
                                armor = new Item("Chain Mail", 4, 100);
                                answered = true;
                                break;
                            case 3:
                                armor = new Item("Plate Armor", 8, 300);
                                answered = true;
                                break;
                            case 4:
                                shop(ref player, random);
                                break;
                            default:
                                Console.WriteLine("Invalid Answer!");
                                Console.ReadLine();
                                break;
                        }
                        if (player.hasArmor == true)
                        {
                            Console.WriteLine("You have an armor currently equipped! Buying this armor will replace the armor you currently have.\nProceed? (Y/N):");
                            string reply = Console.ReadLine();
                            if (reply == "Y" || reply == "y")
                            {
                                if (player.getGold < armor.Cost)
                                {
                                    Console.WriteLine("It seems you do not have enough gold to purchase this item! Earn more and try again.");
                                    answered = false;
                                }
                                else
                                {
                                    player.ArmorEquipped = armor;
                                    player.spendMoney(armor);
                                }
                            }
                            else if (reply == "N" || reply == "n")
                            {
                                answered = false;
                            }
                            else
                            {
                                Console.WriteLine("Invalid Answer!");
                                Console.ReadLine();
                            }
                        }
                        else
                        {
                            Console.WriteLine("Proceed? (Y/N):");
                            string reply = Console.ReadLine();
                            if (reply == "Y" || reply == "y")
                            {
                                if (player.getGold < armor.Cost)
                                {
                                    Console.WriteLine("It seems you do not have enough gold to purchase this item! Earn more and try again.");
                                    answered = false;
                                }
                                else
                                {
                                    player.hasArmor = true;
                                    player.ArmorEquipped = armor;
                                    player.spendMoney(armor);
                                }
                            }
                            else if (reply == "N" || reply == "n")
                            {
                                answered = false;
                            }
                            else
                            {
                                Console.WriteLine("Invalid Answer!");
                                Console.ReadLine();
                            }
                        }

                    }
                }
                else if (answer == 3)
                {
                    willExit = true;
                }
                else
                {
                    Console.WriteLine("Invalid Answer!");
                    Console.ReadLine();
                }
            }
        }
    }
}
