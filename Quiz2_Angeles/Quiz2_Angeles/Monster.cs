﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2_Angeles
{
    class Monster
    {
        public Monster(string Name, int Exp, int Gold, int Hp, int mHp, int pp, int vp, int dp, int ap)
        {
            this.name = Name;
            this.exp = Exp;
            this.gold = Gold;
            this.hp = Hp;
            this.maxHp = mHp;
            this.pow = pp;
            this.vit = vp;
            this.dex = dp;
            this.agi = ap;
        }

        string name;
        public bool isAlive = true;
        public string getName
        {
            get { return name; }
        }
        int exp;
        public int getExp
        {
            get { return exp; }
        }
        int gold;
        public int getGold
        {
            get { return gold; }
        }
        int hp;
        public int getHp
        {
            get { return hp; }
        }
        int maxHp;
        int pow;
        public int getPow
        {
            get { return pow; }
        }
        int vit;
        public int getVit
        {
            get { return vit; }
        }
        int dex;
        public int agi;
        public int getAgi
        {
            get { return agi; }
        }

        public void takeDmg(int dmgTaken)
        {
            int dmg = dmgTaken - this.vit;
            if (dmg <= 0)
            {
                dmg = 1;
            }
            Console.WriteLine(name + " recieved " + dmg + "DMG!");
            this.hp -= dmg;
            if (hp <= 0)
            {
                isAlive = false;
            }
        }
        void randomNumberGenerator(int number, int limit, Random random)
        {
            number = random.Next(0, limit);
        }
        public void attack(Player target, Random rand)
        {
            int dmg;
            int hitRand = 0;

            // Make the dex and agi into floats to avoid below 1 values
            float hitChance = ((float)this.dex / (float)target.agi) * 100;

            // After computation, make sure hitChance is not below 20
            // and is not above 80

            if (hitChance < 20)
            {
                hitChance = 20;
            }
            if (hitChance > 80)
            {
                hitChance = 80;
            }

            // roll a random number from 1-100
            randomNumberGenerator(hitRand, 100, rand);
            // check if hitChance is less than random number
                
            if (hitRand > hitChance)
            {
                Console.WriteLine(name + " attacked " + target.getName);
                dmg = this.pow;
                target.takeDmg(dmg);
            }
            else
            {
                Console.WriteLine(name + " missed!");
            }
        }
    
        public void viewStats()
        {
            Console.WriteLine("Name : " + name );
            Console.WriteLine("HP : " + hp + " / " + maxHp);
            Console.WriteLine("POW : " + pow + "\nVIT : " + vit + "\nDEX : " + dex + "\nAGI : " + agi);
        
        }
        ~Monster()
        {
            Console.WriteLine("The monster disappeared!");
        }
    }
}
