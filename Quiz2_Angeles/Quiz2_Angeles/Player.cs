﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2_Angeles
{
    class Player
    {
        public Player()
        {

        }
        public Player(string Name, string playerClass, int Hp, int mHp, int pp, int vp, int dp, int ap)
        {
            this.name = Name;
            this.title = playerClass;
            this.hp = Hp;
            this.maxHp = mHp;
            this.pow = pp;
            this.vit = vp;
            this.dex = dp;
            this.agi = ap;
        }
        string name;
        public string getName
        {
            get { return name; }
        }
        public List<Item> WeaponEquipped = new List<Item>();
        public List<Item> ArmorEquipped = new List<Item>();
        public bool hasWeapon = false;
        public bool hasArmor = false;

        string title;
        int gold = 10;
        public int getGold
        {
            get { return gold; }
        }
        static int lvl = 1;
        int exp = 0;
        int expCap = 1000;
        int hp;
        public bool isAlive = true;
        public int getHp
        {
            get { return hp; }
        }
        int maxHp;
        int pow;
        int vit;
        int dex;
        public int agi;
        public int getAgi
        {
            get { return agi; }
        }

        int positionX = 0;
        int positionY = 0;
        public int PositionX
        {
            get { return positionX; }
        }
        public int PositionY
        {
            get { return positionY; }
        }

        public void rest()
        {
            hp = maxHp;
        }
        public void takeDmg(int damage)
        {
            int dmg;
            if (hasArmor == true)
            {
                dmg = damage - (this.vit + ArmorEquipped[0].bonus);
                if(dmg <= 0)
                {
                    dmg = 1;
                }
                Console.WriteLine(name + " recieved " + dmg + "DMG!");
                
            }
            else
            {
                dmg = damage - this.vit;
                if (dmg <= 0)
                {
                    dmg = 1;
                }
                Console.WriteLine(name + " recieved " + dmg + "DMG!");
            }
            this.hp -= dmg;
            if(hp <= 0)
            {
                isAlive = false;
            }
        }
        void randomNumberGenerator(int number, int limit, Random random)
        {
            number = random.Next(0, limit);
        }
        public void attack(Monster target,  Random rand)
        {
            int hitRand = rand.Next(1, 100);
            int damage;
            float hitChance = ((float)this.dex / (float)target.agi) * 100;

            // After computation, make sure hitChance is not below 20
            // and is not above 80

            if (hitChance < 20)
            {
                hitChance = 20;
            }
            if (hitChance > 80)
            {
                hitChance = 80;
            }

            // roll a random number from 1-100
            randomNumberGenerator(hitRand, 100, rand);
            // check if hitChance is less than random number
            if (hitRand > hitChance)
            {
                 Console.WriteLine(name + " attacked " + target.getName);
                 if (hasWeapon == true)
                 {
                      damage =(this.pow + WeaponEquipped[0].bonus);
                      target.takeDmg(damage);
                 }
                 else
                 {
                      damage = this.pow;
                      target.takeDmg(damage);
                 }
            }
            else
            {
                 Console.WriteLine(name + " missed!");
            }   
        }
        public void viewStats()
        {
            Console.WriteLine("Name : " + name + " || Title : " + title);
            Console.WriteLine("Level : " + lvl + " || EXP : " + exp + " / " + expCap);
            Console.WriteLine("HP : " + hp + " / " + maxHp);
            Console.WriteLine("POW : " + pow + "\nVIT : " + vit + "\nDEX : " + dex + "\nAGI : " + agi);
            Console.WriteLine("Gold : " + gold);

        }
        public void positionCheck()
        {
            Console.WriteLine("Your current position : " + positionX + "," + positionY);
        }
        public void moveHorizontal(int movement)
        {
            positionX += movement;
        }
        public void moveVertical(int movement)
        {
            positionY += movement;
        }
        public void BattleWon( int earnedExp, int earnedGold, Random rand)
        {
            Console.WriteLine("You earned " + earnedExp + "EXP and " + earnedGold + " Gold!");
            exp += earnedExp;
            gold += earnedGold;
            if(exp == expCap)
            {
                LevelUp(rand);
            }
        }
        public void spendMoney(Item item)
        {
            Console.Write("You have spent " + item.Cost + " Gold on " + item.getName);
            gold -= item.Cost;
            
        }
        void LevelUp(Random rand)
        {
            Console.WriteLine("You have LEVELED UP!");
            lvl++;
            exp = 0;
            expCap = lvl * 1000;
            int number = rand.Next(0, 3);
            Console.WriteLine("You current MAX HP : " + maxHp + " + " + number);
            maxHp += number;
            number = rand.Next(0, 3);
            Console.WriteLine("You current POW : " + pow + " + " + number);
            pow += number;
            number = rand.Next(0, 3);
            Console.WriteLine("You current VIT : " + vit + " + " + number);
            vit += number;
            number = rand.Next(0, 3);
            Console.WriteLine("You current DEX : " + dex + " + " + number);
            dex += number;
            number = rand.Next(0, 3);
            Console.WriteLine("You current AGI : " + agi + " + " + number);
            agi += number;
        }
    }
}
