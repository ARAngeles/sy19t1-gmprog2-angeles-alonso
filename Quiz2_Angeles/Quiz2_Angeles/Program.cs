﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2_Angeles
{
    class Program
    {
        static void Main(string[] args)
        {
            Player user = new Player();
            Random rand = new Random();
            CharacterCreation(rand,ref user);
            

        }

        static void CharacterCreation(Random random,ref Player player)
        {
            bool answered = false;
            Console.WriteLine("Please input a name for your character : ");
            string name = Console.ReadLine();
            Console.WriteLine("Please choose from the three clases :\n1.) Warrior\n2.) Thief\n3.) Crusader");
            while (answered == false)
            {
                int answer = Int32.Parse(Console.ReadLine());
                switch(answer)
                {
                    case 1 :

                        player = new Player(name, "Warrior",13,13,7,4,5,4);
                        DisplayMenu(ref player,random);
                        answered = true;
                        break;
                    case 2:
                        player = new Player(name, "Thief", 10, 10, 4, 3, 6, 6);
                        DisplayMenu(ref player,random);
                        answered = true;
                        break;
                    case 3:
                        player = new Player(name, "Crusader", 15, 15, 4, 6, 4, 3);
                        DisplayMenu(ref player,random);
                        answered = true;
                        break;
                    default:
                        Console.WriteLine("Invalid Answer!");
                        Console.ReadLine();
                        break;
                }
            }           
        }

        static void DisplayMenu(ref Player player,Random random)
        {
            bool answered = false;
            while (answered == false)
            {
                Console.WriteLine("\n====================================\n");
                Console.WriteLine("What would you like to do? : ");
                Console.WriteLine("1.) Move\n2.) Rest\n3.) View Status\n4.) Quit");
                int answer = Int32.Parse(Console.ReadLine());
                switch (answer)
                {
                    case 1:
                        PlayerMove(ref player,random);
                        answered = true;
                        break;
                    case 2:
                        Console.WriteLine(player.getName+" chose to rest for this turn.\nYou are fully rested! Your HP has is now at max");
                        player.rest();
                        break;
                    case 3:
                        player.viewStats();
                        player.positionCheck();
                        Console.ReadLine();
                        break;
                    case 4:
                        System.Environment.Exit(0);
                        answered = true;
                        break;
                    default:
                        Console.WriteLine("Invalid Answer!");
                        Console.ReadLine();
                        break;
                }
            }
        }

        static void PlayerMove(ref Player player,Random random)
        {
            bool answered = false;
            while (answered == false)
            {
                Console.WriteLine("\n====================================\n");
                Console.WriteLine("Where would you like to go? : ");
                Console.WriteLine("1.) North\n2.) South\n3.) East\n4.) West");
                int answer = Int32.Parse(Console.ReadLine());
                switch (answer)
                {
                    case 1:
                        Console.WriteLine(player.getName + " chose to go North");
                        player.moveVertical(1);
                        answered = true;
                        break;
                    case 2:
                        Console.WriteLine(player.getName + " chose to go South");
                        player.moveVertical(-1);
                        answered = true;
                        break;
                    case 3:
                        Console.WriteLine(player.getName + " chose to go East");
                        player.moveHorizontal(1);
                        answered = true;
                        break;
                    case 4:
                        Console.WriteLine(player.getName + " chose to go West");
                        player.moveHorizontal(-1);
                        answered = true;
                        break;
                    default:
                        Console.WriteLine("Invalid Answer!");
                        Console.ReadLine();
                        break;
                }
                
            }
            encounterCheck(ref player, random);
        }

        static void encounterCheck(ref Player player, Random random)
        {
            
            if (player.PositionX == 1 && player.PositionY == 1)
            {
                bool answered = false;
                while (answered == false)
                {
                    Console.WriteLine("\n====================================\n");
                    Console.WriteLine("You landed in the shop!\nWould you like to shop? (Y/N) :");
                    string answer = Console.ReadLine();
                    if(answer == "Y" || answer == "y")
                    {
                        shop(ref player,random);
                    }
                    else if(answer == "N" || answer == "n")
                    {
                        DisplayMenu(ref player, random);
                    }
                    else
                    {
                        Console.WriteLine("Invalid Answer!");
                        Console.ReadLine();
                    }
                
                }
                    
            }
            else
            {
                int encounter = random.Next(0, 100);
                if(encounter < 20)
                {
                    Console.WriteLine("\nNothing happend!");
                    DisplayMenu(ref player, random);
                }
                else if (encounter < 20 + 25)
                {
                    Monster monster = new Monster("Goblin", 100, 10, 10, 10, 2, 1, 3, 3);
                    Console.WriteLine("\nYou have encountered a " + monster.getName);
                    battleLoop(ref player,ref monster, random);
                }
                else if (encounter < 20 + 25 + 25)
                {
                    Monster monster = new Monster("Ogre", 250, 50, 35, 35, 6, 7, 5, 2);
                    Console.WriteLine("\nYou have encountered a " + monster.getName);
                    battleLoop(ref player, ref monster, random);
                }
                else if (encounter < 20 + 25 + 25 + 25)
                {
                    Monster monster = new Monster("Orc", 500, 100, 50, 50, 8, 6, 7, 6);
                    Console.WriteLine("\nYou have encountered a " + monster.getName);
                    battleLoop(ref player, ref monster, random);
                }
                else 
                {
                    Monster monster = new Monster("Orc Lord", 1000, 1000, 100, 100, 15, 10, 13, 10);
                    Console.WriteLine("\nYou have encountered a " + monster.getName);
                    battleLoop(ref player, ref monster, random);
                }

            }
        }
        static void battleLoop(ref Player player, ref Monster monster , Random random)
        {
            bool running = false;
            while(player.isAlive  == true && monster.isAlive == true && running == false)
            {
                
                bool answered = false;
                Console.WriteLine("\n====================================\n");
                monster.viewStats();
                Console.WriteLine("\n====================================\n");
                player.viewStats();
                while(answered == false)
                {
                    
                    Console.WriteLine("What would you like to do? : ");
                    Console.WriteLine("1.) Attack\n2.) Run");
                    int answer = Int32.Parse(Console.ReadLine());
                    switch(answer)
                    {
                        case 1:
                            player.attack(monster,random);
                            Console.WriteLine("\n====================================\n");
                            monster.attack(player,random);
                            Console.ReadLine();
                            answered = true;
                            break;
                        case 2:
                            int runChance = random.Next(0, 100);
                            if(runChance < 25)
                            {
                                Console.WriteLine("You have successfully escaped!");
                                running = true;
                                answered = true;
                                Console.ReadLine();

                            }
                            else
                            {
                                Console.WriteLine("You have failed to escape!");
                                Console.WriteLine("\n====================================\n");
                                monster.attack(player,random);
                                answered = true;
                                Console.ReadLine();
                            }
                            break;
                        default:
                            Console.WriteLine("Invalid Answer!");
                            Console.ReadLine();
                            break;
                    }
                }
            }
            if(player.isAlive == false)
            {
                Console.WriteLine("You have died!\n==== G A M E   O V E R ====");
                System.Environment.Exit(0);
            }
            else if(monster.isAlive == false)
            {
                Console.WriteLine("You have successfully defeated the " + monster.getName + "!");
                player.BattleWon( monster.getExp, monster.getGold,random);
                DisplayMenu(ref player, random);
            }
            else
            {
                DisplayMenu(ref player, random);
            }

        }
        static void shop(ref Player player,Random random)
        {
            bool willExit = false;
            Console.WriteLine("Welcome to the Shop!");
            while(willExit == false)
            {
                Console.WriteLine("\n====================================\n");
                Console.WriteLine("What would you like to do? : ");
                Console.WriteLine("1.) Buy Weapons\n2.) Buy Armor\n3.) Leave Shop");
                int answer = Int32.Parse(Console.ReadLine());
                if (answer == 1)
                {
                    bool answered = false;
                    while (answered == false)
                    {
                        Console.WriteLine("\n====================================\n");
                        Console.WriteLine("What weapon would you like to buy?? : ");
                        Item weapon = new Item();
                        Console.WriteLine("1.) Shortsword\n2.) Longsword\n3.) Broad Sword\n4.) Excalibur\n5.) Back");
                        int response = Int32.Parse(Console.ReadLine());
                        switch (response)
                        {
                            case 1:
                                weapon = new Item("Shortsword", 5, 10);
                                answered = true;
                                break;
                            case 2:
                                weapon = new Item("Longsword", 10, 50);
                                answered = true;
                                break;
                            case 3:
                                weapon = new Item("Broad Sword", 20, 200);
                                answered = true;
                                break;
                            case 4:
                                weapon = new Item("Excalibur", 999, 9999);
                                answered = true;
                                break;
                            case 5:
                                shop(ref player, random);
                                break;
                            default:
                                Console.WriteLine("Invalid Answer!");
                                Console.ReadLine();
                                break;
                        }
                        if (player.hasWeapon == true)
                        {
                            Console.WriteLine("You have a weapon currently equipped! Buying this weapon will replace the weapon you currently have.\nProceed? (Y/N):");
                            string reply = Console.ReadLine();
                            if (reply == "Y" || reply == "y")
                            {
                                if(player.getGold < weapon.Cost)
                                {
                                    Console.WriteLine("It seems you do not have enough gold to purchase this item! Earn more and try again.");
                                    answered = false;
                                }
                                else
                                {
                                    player.WeaponEquipped.Clear();
                                    player.WeaponEquipped.Add(weapon);
                                    player.spendMoney(weapon);
                                }
                                
                            }
                            else if (reply == "N" || reply == "n")
                            {
                                answered = false;
                            }
                            else
                            {
                                Console.WriteLine("Invalid Answer!");
                                Console.ReadLine();
                            }
                        }
                        else
                        {
                            Console.WriteLine("Proceed? (Y/N):");
                            string reply = Console.ReadLine();
                            if (reply == "Y" || reply == "y")
                            {
                                if (player.getGold < weapon.Cost)
                                {
                                    Console.WriteLine("It seems you do not have enough gold to purchase this item! Earn more and try again.");
                                    answered = false;
                                }
                                else
                                {
                                    player.hasWeapon = true;
                                    player.WeaponEquipped.Add(weapon);
                                    player.spendMoney(weapon);
                                }                                
                            }
                            else if (reply == "N" || reply == "n")
                            {
                                answered = false;
                            }
                            else
                            {
                                Console.WriteLine("Invalid Answer!");
                                Console.ReadLine();
                            }
                        }
                    }

                }
                else if (answer == 2)
                {
                    bool answered = false;
                    while (answered == false)
                    {
                        Console.WriteLine("\n====================================\n");
                        Console.WriteLine("What armor would you like to buy?? : ");
                        Item armor = new Item();
                        Console.WriteLine("1.) Leather Mail\n2.) Chain Mail\n3.) Plate Armor\n4.) Back");
                        int response = Int32.Parse(Console.ReadLine());
                        switch (response)
                        {
                            case 1:
                                armor = new Item("Leather Mail", 2, 50);
                                answered = true;
                                break;
                            case 2:
                                armor = new Item("Chain Mail", 4, 100);
                                answered = true;
                                break;
                            case 3:
                                armor = new Item("Broad Sword", 8, 300);
                                answered = true;
                                break;
                            case 4:
                                shop(ref player, random);
                                break;
                            default:
                                Console.WriteLine("Invalid Answer!");
                                Console.ReadLine();
                                break;
                        }
                        if (player.hasArmor == true)
                        {
                            Console.WriteLine("You have an armor currently equipped! Buying this armor will replace the armor you currently have.\nProceed? (Y/N):");
                            string reply = Console.ReadLine();
                            if (reply == "Y" || reply == "y")
                            {
                                if (player.getGold < armor.Cost)
                                {
                                    Console.WriteLine("It seems you do not have enough gold to purchase this item! Earn more and try again.");
                                    answered = false;
                                }
                                else
                                {
                                    player.ArmorEquipped.Clear();
                                    player.ArmorEquipped.Add(armor);
                                    player.spendMoney(armor);
                                }
                            }
                            else if (reply == "N" || reply == "n")
                            {
                                answered = false;
                            }
                            else
                            {
                                Console.WriteLine("Invalid Answer!");
                                Console.ReadLine();
                            }
                        }
                        else
                        {
                            Console.WriteLine("Proceed? (Y/N):");
                            string reply = Console.ReadLine();
                            if (reply == "Y" || reply == "y")
                            {
                                if (player.getGold < armor.Cost)
                                {
                                    Console.WriteLine("It seems you do not have enough gold to purchase this item! Earn more and try again.");
                                    answered = false;
                                }
                                else
                                {
                                    player.hasArmor = true;
                                    player.ArmorEquipped.Add(armor);
                                    player.spendMoney(armor);
                                }
                            }
                            else if (reply == "N" || reply == "n")
                            {
                                answered = false;
                            }
                            else
                            {
                                Console.WriteLine("Invalid Answer!");
                                Console.ReadLine();
                            }
                        }

                    }
                }
                else if (answer == 3)
                {
                    DisplayMenu(ref player, random);
                    willExit = true;
                }
                else
                {
                    Console.WriteLine("Invalid Answer!");
                    Console.ReadLine();
                }
            }
        }
    }
}
