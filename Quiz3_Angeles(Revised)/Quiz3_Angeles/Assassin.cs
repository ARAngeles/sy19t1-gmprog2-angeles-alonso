﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3_Angeles
{
    class Assassin : Character
    {
        Random rand = new Random();
        public Assassin()
        {
            hp = rand.Next(40, 45);
            maxHp = hp;
            mp = rand.Next(40, 50);
            pow = rand.Next(20, 25);
            vit = rand.Next(20, 25);
            dex = rand.Next(30, 35);
            agi = rand.Next(30, 35);           
        }
        string title = "Assassin";
        string skillName = "Assassinate";
        int classNumber = 2;
        int hp;
        int maxHp;
        int pow;
        int vit;
        int dex;
        int agi;
        int mp;
        int mpCost = 4;

        public override void skill(Character target,Random random)
        {

            float dmg = ((float)this.pow * 2.2f);
            target.takeDmg((int)dmg, target.classNumber, random);
        }
    }
}
