﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3_Angeles
{
    class Character
    { 
        string name;
        public string getName
        {
            get { return name; }
        }
        string title;
        public string getTitle
        {
            get { return title; }
        }
        string skillName;
        public string getSkillName
        {
            get { return skillName; }
        }
        public int classNumber;
        int hp;
        public int getHp
        {
            get { return hp; }
        }
        int maxHp;
        public int getMaxHp
        {
            get { return maxHp; }
        }
        int mp;
        public int getMp
        {
            get { return mp; }
        }
        int pow;
        public int getPow
        {
            get { return pow; }
        }
        int vit;
        public int getVit
        {
            get { return vit; }
        }
        int dex;
        public int getDex
        {
            get { return dex; }
        }
        int agi;
        public int getAgi
        { get { return agi; } }
        bool isAlive = true;
        public void viewStats()
        {
            Console.WriteLine("Name : " + name + " || Title : " + title);
            Console.WriteLine("HP : " + hp + " / " + maxHp);
            Console.WriteLine("MP : " + mp);
            Console.WriteLine("POW : " + pow + "\nVIT : " + vit + "\nDEX : " + dex + "\nAGI : " + agi);
        }
        void randomNumberGenerator(int number, int limit, Random random)
        {
            number = random.Next(0, limit);
        }
        public void attack(Character target, Random random)
        {
            float dmg;
            int hitRand = 0;

            // Make the dex and agi into floats to avoid below 1 values
            float hitChance = ((float)this.dex / (float)target.agi) * 100;

            // After computation, make sure hitChance is not below 20
            // and is not above 80

            if (hitChance < 20)
            {
                hitChance = 20;
            }
            if (hitChance > 80)
            {
                hitChance = 80;
            }

            // roll a random number from 1-100
            randomNumberGenerator(hitRand, 100, random);
            // check if hitChance is less than random number

            if (hitRand > hitChance)
            {
                Console.WriteLine(name + " attacked " + target.getName);
                dmg = (float)this.pow;
                target.takeDmg((int)dmg, target.classNumber, random);
            }
            else
            {
                Console.WriteLine(name + " missed!");
            }
        }
        public void recover(int heal)
        {
            Console.WriteLine(name + " healed back " + heal + "HP!");
            hp += heal;
        }
        public void takeDmg(int damage, int atkClassNo ,Random random)
        {
            int critRand = 0;
            float dmg = 0;
            randomNumberGenerator(critRand, 100, random);
            if ((this.classNumber == 1 && atkClassNo == 2) || (this.classNumber == 2 && atkClassNo == 3) || (this.classNumber == 3 && atkClassNo == 1))
            {
                if (critRand > 20)
                {
                    dmg = ((damage * 0.5f) - this.vit) * 0.2f;
                }
                else
                {
                    dmg = ((damage * 0.5f) - this.vit);
                }
            }
            else if ((this.classNumber == 1 && atkClassNo == 3) || (this.classNumber == 2 && atkClassNo == 1) || (this.classNumber == 3 && atkClassNo == 2))
            {
                if (critRand > 20)
                {
                    dmg = ((damage/2) - this.vit) * 0.2f;
                }
                else
                {
                    dmg = ((damage / 2) - this.vit);
                }
            }
            else
            {
                if (critRand > 20)
                {
                    dmg = (damage - this.vit)* 0.2f;
                }
                else
                {
                    dmg = damage - this.vit;
                }
            }
            if (critRand > 20)
            {
                Console.WriteLine(name + " took CRIT " + dmg + "DMG!");
            }
            else
            {
                Console.WriteLine(name + " took " + dmg + "DMG!");
            }
            hp -= (int)dmg;
            if(hp <=0)
            {
                isAlive = false;
            }
        }
        public virtual void skill(Character target,Random random)
        {
            Console.WriteLine(name + " used a skill!");
        }
            
    }
}
