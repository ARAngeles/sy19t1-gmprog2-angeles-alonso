﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3_Angeles
{
    class Mage : Character
    {
        Random rand = new Random();
        public Mage()
        {
            hp = rand.Next(30, 40);
            maxHp = hp;
            mp = rand.Next(55, 65);
            pow = rand.Next(30, 35);
            vit = rand.Next(20, 25);
            dex = rand.Next(30, 35);
            agi = rand.Next(20, 25);
        }
        string title = "Mage";
        string skillName = "Heal";
        int classNumber = 3;
        int hp;
        int maxHp;
        int pow;
        int vit;
        int dex;
        int agi;
        int mp;
        int mpCost = 5;

        public override void skill(Character target,Random random)
        {
            float heal = ((float)target.getHp * 0.3f);
            target.recover((int)heal);
        }
    }
}
