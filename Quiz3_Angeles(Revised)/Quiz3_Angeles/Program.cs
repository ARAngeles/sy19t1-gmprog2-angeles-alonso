﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3_Angeles
{
    class Program
    {
        

        static void Main(string[] args)
        {
            List<Character> playerList = new List<Character>();
            for(int x =0;x<3;x++)
            {
                CharacterCreation(ref playerList);
            }
            for(int y =0;y<3;y++)
            {
                playerList[y].viewStats();
                Console.WriteLine("\n");
            }
        }
        static void CharacterCreation(ref List<Character> characters)
        {
            Character player = new Character();
            bool answered = false;
            Console.WriteLine("Please input a name for a character : ");
            string name = Console.ReadLine();
            Console.WriteLine("Please choose from the three clases :\n1.) Warrior\n2.) Assassin\n3.) Mage");
            while (answered == false)
            {
                int answer = Int32.Parse(Console.ReadLine());
                switch (answer)
                {
                    case 1:
                        player = new Warrior();
                        answered = true;
                        break;
                    case 2:
                        player = new Assassin();
                        answered = true;
                        break;
                    case 3:
                        player = new Mage();
                        answered = true;
                        break;
                    default:
                        Console.WriteLine("Invalid Answer!");
                        Console.ReadLine();
                        break;
                }
            }
                characters.Add(player);
     
            return;
        }
    }
}
