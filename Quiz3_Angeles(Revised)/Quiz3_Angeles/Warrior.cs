﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3_Angeles
{
    class Warrior : Character
    {
        Random rand = new Random();
        public Warrior()
        {
            hp = rand.Next(55, 65);
            maxHp = hp;
            mp = rand.Next(30, 40);
            pow = rand.Next(20, 30);
            vit = rand.Next(30, 35);
            dex = rand.Next(20, 25);
            agi = rand.Next(20, 25);
        }
        string title = "Warrior";
        string skillName = "Shockwave";
        int classNumber = 1;
        int hp;
        int maxHp;
        int pow;
        int vit;
        int dex;
        int agi;
        int mp;
        int mpCost = 5;

        public override void skill(Character target,Random random)
        {
            float dmg = ((float)this.pow * 0.9f);
            target.takeDmg((int)dmg,target.classNumber,random);
        }
    }
}
