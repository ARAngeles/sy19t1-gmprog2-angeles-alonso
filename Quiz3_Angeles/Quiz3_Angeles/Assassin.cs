﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3_Angeles
{
    class Assassin : Character
    {
        string title = "Assassin";
        string skillName = "Assassinate";
        int classNumber = 2;
        int pow;
        int mp;
        int mpCost = 4;

        public override void skill(Character target,Random random)
        {

            float dmg = ((float)this.pow * 2.2f);
            target.takeDmg((int)dmg, target.classNumber, random);
        }
    }
}
