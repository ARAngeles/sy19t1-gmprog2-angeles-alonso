﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3_Angeles
{
    class Mage : Character
    {
        string title = "Mage";
        string skillName = "Heal";
        int classNumber = 3;
        int pow;
        int mp;
        int mpCost = 5;

        public override void skill(Character target,Random random)
        {
            float heal = ((float)target.getHp * 0.3f);
            target.recover((int)heal);
        }
    }
}
