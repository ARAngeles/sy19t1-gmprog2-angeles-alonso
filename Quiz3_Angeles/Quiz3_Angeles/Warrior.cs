﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3_Angeles
{
    class Warrior : Character
    {
        public Warrior()
        {

        }
        string title = "Warrior";
        string skillName = "Shockwave";
        int classNumber = 1;
        int pow;
        int mp;
        int mpCost = 5;

        public override void skill(Character target,Random random)
        {
            float dmg = ((float)this.pow * 0.9f);
            target.takeDmg((int)dmg,target.classNumber,random);
        }
    }
}
