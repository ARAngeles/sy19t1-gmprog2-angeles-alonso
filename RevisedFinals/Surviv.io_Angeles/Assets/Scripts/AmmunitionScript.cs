﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmunitionScript : MonoBehaviour
{
    SpriteRenderer ammoSprite;
    int amount;
    public int ammotype;
    Random rand = new Random();

    // Start is called before the first frame update
    void Start()
    {
        ammotype = Random.Range(0, 3);
        ammoSprite = GetComponent<SpriteRenderer>();
        if (ammotype == 1)
        {
            ammoSprite.color = Color.red;
            amount = Random.Range(6, 24);
        }
        else if (ammotype == 2)
        {
            ammoSprite.color = Color.blue;
            amount = Random.Range(30, 90);
        }
        else
        {
            ammoSprite.color = Color.green;
            amount = Random.Range(4, 16);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {

        if (other.gameObject.tag == "Player")
        {
            GameManagerScript.Instance.RemoveObjects(this.gameObject);
            GameManagerScript.Instance.addAmmo(ammotype, amount);

        }
    }
}
