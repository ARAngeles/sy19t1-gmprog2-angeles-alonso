﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public int damage;
    [SerializeField] private PlayerScript player;
    [SerializeField] private EnemyScript enemy;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log(other.gameObject.name);


        if (other.gameObject.tag == "Player")
        {
            GameManagerScript.Instance.RemoveObjects(this.gameObject);
            player.TakeDMG(damage);
        }
        else if (other.gameObject.tag == "Enemy")
        {
            GameManagerScript.Instance.RemoveObjects(this.gameObject);
            enemy.TakeDMG(damage);
        }
        else if (other.gameObject.tag == "Object")
        {
            GameManagerScript.Instance.RemoveObjects(this.gameObject);
        }
    }
}
