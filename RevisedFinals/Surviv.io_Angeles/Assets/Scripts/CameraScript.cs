﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField] private PlayerScript playerScript;
    float moveSpd = 5f;
    public GameObject player;
    private Vector3 offset;
    Vector3 direction;
    // Start is called before the first frame update
    void Start()
    {
        offset = new Vector3(0, 0, -20);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(player.transform.position.x + offset.x, player.transform.position.y + offset.y, offset.z);

    }

    void OnGUI()
    {
        if(playerScript.hasWeapon == true)
        {
            if (playerScript.weaponScript.type == 1)
            {
                GUI.Label(new Rect(2.63f, 3.5f, 150, 50), "Ammo: " + playerScript.weaponScript.ammo + " /  " + playerScript.pistolAmmo);
            }
            else if (playerScript.weaponScript.type == 2)
            {
                GUI.Label(new Rect(2.63f, 3.5f, 150, 50), "Ammo: " + playerScript.weaponScript.ammo + " /  " + playerScript.rifleAmmo);
            }
            else if (playerScript.weaponScript.type == 3)
            {
                GUI.Label(new Rect(2.63f, 3.5f, 150, 50), "Ammo: " + playerScript.weaponScript.ammo + " /  " + playerScript.shotgunAmmo);
            }
        }
        else
        {
            GUI.Label(new Rect(2.63f, 3.5f, 150, 50), "None");
        }
    }
       
}
