﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    Rigidbody2D rb;
    SpriteRenderer enemySprite;
    public CircleCollider2D detector;
    public GameObject obj;
    float hp = 1f;

    public Color[] colorarray;
    // Start is called before the first frame update
    void Start()
    {
        enemySprite = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        int colorNumber = Random.Range(0, 3);
        enemySprite.color = colorarray[colorNumber];
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = obj.transform.position;
        if (hp == 0)
        {
            GameManagerScript.Instance.RemoveObjects(this.gameObject);
        }
    }

    public void TakeDMG(float damage)
    {
        hp -= damage;
    }

}
