﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public Transform minSpawnPoint;
    public Transform maxSpawnPoint;
    public int maxSpawned;
    int spawned;
    public int spawnLimit;
    int objCount = 0;
    public GameObject objects;
    public List<GameObject> objectList;
    public float spawnTime = 2;
    private float curSpawnTime;

    void Start()
    {
        curSpawnTime = spawnTime;
        for (int i = 0; i < maxSpawned; i++)
        {

            SpawnObjects();
            spawned++;
            objCount++;
            objectList.Add(objects);

        }

        //create a loop that spawn 30 food during initialization
        // increase your food counter when you spawn a food
    }

    void Update()
    {

        curSpawnTime -= 1 * Time.deltaTime;
        if (objCount < spawnLimit)
        {
            if (spawned < maxSpawned)
            {
                if (curSpawnTime <= 0)
                {

                    SpawnObjects();
                    objectList.Add(objects);
                    spawned++;
                    objCount++;
                    curSpawnTime = spawnTime;
                }
            }
        }

        //create a counter for food if you reach the max food count stop spawning of food
        //create a timer that spawn a food
    }

    void SpawnObjects()
    {
        float x = Random.Range(minSpawnPoint.position.x, maxSpawnPoint.position.x);
        float y = Random.Range(minSpawnPoint.position.y, maxSpawnPoint.position.y);
        Vector3 spwnPos = new Vector3(x, y, 0);
        var newObjs = GameObject.Instantiate(objects, spwnPos, Quaternion.identity);
    }
}
