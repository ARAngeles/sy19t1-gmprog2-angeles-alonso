﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : Singleton<GameManagerScript>
{
    [SerializeField] private PlayerScript player;
    public Transform minSpawnPoint;
    public Transform maxSpawnPoint;
    public int maxSpawned;
    int objCount = 0;
    public GameObject ammo;
    public GameObject pistol;
    public GameObject rifle;
    public GameObject shotgun;
    public GameObject objects;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (objCount <= maxSpawned)
        {
           SpawnObjects();
           SpawnAmmo();
            SpawnGuns();
            objCount++;
        }

    }

    void SpawnObjects()
    {
        float x = Random.Range(minSpawnPoint.position.x, maxSpawnPoint.position.x);
        float y = Random.Range(minSpawnPoint.position.y, maxSpawnPoint.position.y);
        Vector3 spwnPos = new Vector3(x, y, 0);
        var newObjs = GameObject.Instantiate(objects, spwnPos, Quaternion.identity);
    }
    void SpawnAmmo()
    {
        float x = Random.Range(minSpawnPoint.position.x, maxSpawnPoint.position.x);
        float y = Random.Range(minSpawnPoint.position.y, maxSpawnPoint.position.y);
        Vector3 spwnPos = new Vector3(x, y, 0);
        var newObjs = GameObject.Instantiate(ammo, spwnPos, Quaternion.identity);
    }
    void SpawnGuns()
    {
        float x = Random.Range(minSpawnPoint.position.x, maxSpawnPoint.position.x);
        float y = Random.Range(minSpawnPoint.position.y, maxSpawnPoint.position.y);
        int type = Random.Range(0, 3);
        Vector3 spwnPos = new Vector3(x, y, 0);
        if(type ==1)
        {
            var newObjs = GameObject.Instantiate(pistol, spwnPos, Quaternion.identity);
        }
        else if (type ==2)
        {
            var newObjs = GameObject.Instantiate(rifle, spwnPos, Quaternion.identity);
        }
        else
        {
            var newObjs = GameObject.Instantiate(shotgun, spwnPos, Quaternion.identity);
        }
        
    }
    public void RemoveObjects(GameObject objects)
    {
        Destroy(objects.gameObject);
    }
    public void RemoveCollider(CircleCollider2D collider)
    {
        Destroy(collider);
    }
    public void addAmmo(int type, int amount)
    {
        player.pickUpAmmo(type,amount);
    }
    
    public void PickupWeapon(WeaponScript weaponScript)
    {
        player.pickUpWeapon(weaponScript);
    }
}
