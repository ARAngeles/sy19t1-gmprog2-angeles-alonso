﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{

    [SerializeField] private HealthBarScript healthBar;
    [SerializeField] public WeaponScript weaponScript;
    public GameObject weapon;
    float moveSpd = 10f;
    public Rigidbody2D rb;
    SpriteRenderer playerSprite;
    Vector2 movement;
    float firerate = 10f;
    float lastfired;
    public bool hasWeapon = false;
    public Color[] colorarray;
    public Transform minBorderPoint;
    public Transform maxBorderPoint;
    float hp = 1f;
    public GameObject pistolBullet;
    public GameObject rifleBullet;
    public GameObject shotgunBullet;
    public int pistolAmmo = 0;
    public int rifleAmmo = 0;
    public int shotgunAmmo = 0;
    public float angle;
    // Start is called before the first frame update
    void Start()
    {
        playerSprite = GetComponent<SpriteRenderer>();
        int colorNumber = Random.Range(0, 5);
        playerSprite.color = colorarray[colorNumber];
    }

    // Update is called once per frame
    void Update()
    {
        //virtual bool keyPressed();
        movement.x = Input.GetAxis("Horizontal");
        movement.y = Input.GetAxis("Vertical");



        Vector3 mousePos = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 dir = Input.mousePosition - mousePos;
        angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        

        if(hp <= .5f)
        {
            healthBar.SetColor(Color.yellow);
        }
        else if(hp <= .25f)
        {
            healthBar.SetColor(Color.red);
        }
        else if (hp > .5f)
        {
            healthBar.SetColor(Color.white);
        }

        //Vector3 direction = new Vector3(0, 0, dir.z);
        //transform.LookAt(dir, Vector3.forward);
    }
    void FixedUpdate()
    {
        //Debug.Log(movement);
        rb.MovePosition(rb.position + movement * moveSpd * Time.fixedDeltaTime);

        if (Input.GetKey(KeyCode.KeypadPlus))
        {
            hp += 0.01f;
            healthBar.SetSize(hp);
        }
        if (Input.GetKey(KeyCode.KeypadMinus))
        {
            hp -= 0.01f;
            healthBar.SetSize(hp);
        }

        if(hasWeapon == true)
        {
            if(weaponScript.ammo > 0)
            {
                if (weaponScript.type == 2)
                {
                    if (Input.GetButton("Fire1"))
                    {
                        if (Time.time - lastfired > 1 / firerate)
                        {
                            lastfired = Time.time;
                            weaponScript.FireBullet();
                            weaponScript.ammo--;
                        }
                    }
                }
                else
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        weaponScript.FireBullet();
                        weaponScript.ammo--;
                    }
                }
            }
            
            if(weaponScript.type == 1 && pistolAmmo > 0)
            {
                if (Input.GetKeyDown(KeyCode.R))
                {
                    Reload();
                }
            }
            else if (weaponScript.type == 2 && rifleAmmo > 0)
            {
                if (Input.GetKeyDown(KeyCode.R))
                {
                    Reload();
                }
            }
            else if (weaponScript.type == 1 && shotgunAmmo > 0)
            {
                if (Input.GetKeyDown(KeyCode.R))
                {
                    Reload();
                }
            }
            
        }
        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    rb.AddForce(Vector2.left);
        //}

        //if (Input.GetKeyDown(KeyCode.D))
        //{
        //    rb.AddForce(Vector2.right);
        //}
        //if (Input.GetKeyDown(KeyCode.W))
        //{
        //    rb.AddForce(Vector2.up);
        //}
        //if (Input.GetKeyDown(KeyCode.S))
        //{
        //    rb.AddForce(Vector2.down);
        //}
        if (hp == 0)
        {
            GameManagerScript.Instance.RemoveObjects(this.gameObject);
        }

    }

    public void pickUpAmmo(int type, int amount)
    {
        if(type == 1)
        {
            pistolAmmo += amount;
        }
        else if(type == 2)
        {
            rifleAmmo += amount;
        }
        else
        {
            shotgunAmmo += amount;
        }
    }

    public void Reload()
    {
        if(weaponScript.type == 1)
        {
            if (pistolAmmo < weaponScript.capacity && pistolAmmo > 0)
            {
                weaponScript.ammo = pistolAmmo;
                pistolAmmo = 0;
            }
            else
            {
                pistolAmmo -= weaponScript.capacity;
                weaponScript.ammo = weaponScript.capacity;
            }
        }
        else if (weaponScript.type ==2)
        {
            if (rifleAmmo < weaponScript.capacity && rifleAmmo > 0)
            {
                weaponScript.ammo = rifleAmmo;
                rifleAmmo = 0;
            }
            else
            {
                rifleAmmo -= weaponScript.capacity;
                weaponScript.ammo = weaponScript.capacity;
            }
        }
        else
        {
            if (shotgunAmmo < weaponScript.capacity && shotgunAmmo > 0)
            {
                shotgunAmmo = 0;
                weaponScript.ammo = shotgunAmmo;
            }
            else
            {
                shotgunAmmo -= weaponScript.capacity;
                weaponScript.ammo = weaponScript.capacity;
            }
        }
    }
    public void TakeDMG(float damage)
    {
        hp -= damage;
    }
    public void pickUpWeapon(WeaponScript weaponScript)
    {
        hasWeapon = true;
        this.weaponScript.type = weaponScript.type;
        this.weaponScript.capacity = weaponScript.capacity;
        this.weaponScript.ammo = weaponScript.ammo;
        if(weaponScript.type == 1)
        {
            this.weaponScript.bullet = pistolBullet;
        }
        else if (weaponScript.type == 2)
        {
            this.weaponScript.bullet = rifleBullet;
        }
        else if (weaponScript.type == 3)
        {
            this.weaponScript.bullet = shotgunBullet;
        }
    }
}
