﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponScript : MonoBehaviour
{
    public Transform barrel;
    public int type;
    [SerializeField] private PlayerScript player;
    public GameObject bullet;
    public int capacity;
    public int ammo;
    public float spd = 20f;
    Quaternion bulletRotation;
    float spread;
    // Update is called once per frame
    void Update()
    {
        spread = Random.Range(-3f, 3f);
    }

    public void FireBullet()
    {

        if (type == 1)
        {
            GameObject firedBullet = Instantiate(bullet, barrel.position, barrel.rotation);
            Rigidbody2D rb = firedBullet.GetComponent<Rigidbody2D>();
            rb.AddForce(barrel.up * spd, ForceMode2D.Impulse);
        }
        else if (type == 3)
        {
            var startAngle = -Mathf.FloorToInt((5 - 1) / 2) * 5;
            for (var i = 0; i < 5; i++, startAngle += 15)
            {
                GameObject firedBullet = Instantiate(bullet, barrel.position, Quaternion.AngleAxis(startAngle, transform.up) * barrel.rotation);
                Rigidbody2D rb = firedBullet.GetComponent<Rigidbody2D>();
                rb.AddForce(barrel.up * spd, ForceMode2D.Impulse);
            }
        }
        else if (type == 2)
        {
            
            GameObject firedBullet = Instantiate(bullet, barrel.position, Quaternion.AngleAxis(spread, transform.up) * barrel.rotation);
            firedBullet.GetComponent<Rigidbody2D>();
            Rigidbody2D rb = firedBullet.GetComponent<Rigidbody2D>();
            rb.AddForce(barrel.up * spd, ForceMode2D.Impulse);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(player.hasWeapon == true)
        {
            if (other.gameObject.tag == "Player")
            {
                if (Input.GetKeyDown(KeyCode.E))
                {
                    GameManagerScript.Instance.PickupWeapon(this);
                    GameManagerScript.Instance.RemoveObjects(this.gameObject);
                }
            }
        }
        else
        {
            if (other.gameObject.tag == "Player")
            {
                GameManagerScript.Instance.PickupWeapon(this);
                GameManagerScript.Instance.RemoveObjects(this.gameObject);
                player.hasWeapon = true;
            }
        }
        
    }

}
