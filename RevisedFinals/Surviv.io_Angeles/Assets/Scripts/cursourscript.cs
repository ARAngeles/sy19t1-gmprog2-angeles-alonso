﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cursourscript : MonoBehaviour
{
    float moveSpd = 50f;
    Vector3 mousePos;
    Rigidbody2D rb;
    Vector2 direction;
    public float size = 0;
    float parameter;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = Mathf.Clamp(mousePos.z, 0, 0);
        transform.position = Vector3.MoveTowards(transform.position, mousePos, (moveSpd / this.transform.localScale.x) * Time.deltaTime);
    }
}
