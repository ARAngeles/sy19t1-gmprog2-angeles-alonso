﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmunitionScript : MonoBehaviour
{
    SpriteRenderer ammoSprite;
    float amount;
    public int ammotype;
    Random rand = new Random();

    // Start is called before the first frame update
    void Start()
    {
        ammotype = Random.Range(1, 3);

        if (ammotype == 1)
        {
            ammoSprite.color = Color.red;
            amount = Random.Range(6, 24);
        }
        else if ( ammotype== 2)
        {
            ammoSprite.color = Color.blue;
            amount = Random.Range(30, 90);
        }
        else
        {
            ammoSprite.color = Color.green;
            amount = Random.Range(4, 16);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
