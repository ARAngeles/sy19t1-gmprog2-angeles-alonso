﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    float moveSpd = 5f;
    public GameObject player;
    private Vector3 offset;
    Vector3 direction;
    // Start is called before the first frame update
    void Start()
    {
        offset = new Vector3(0, 0, -20);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(player.transform.position.x + offset.x, player.transform.position.y + offset.y, offset.z);

    }
}
