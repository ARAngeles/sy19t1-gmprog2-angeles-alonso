﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectionScript : MonoBehaviour
{
    float moveSpd = 5f;
    Vector2 place;
    Vector3 movement;
    Vector3 border;
    GameObject unit;
    public GameObject body;
    bool isColliding = false;
    Vector3 escape;
    bool safe = true;
    bool chase = false;
    public float locationTime = 3;
    private float curLocationTime;
    int offset = 5;

    // Start is called before the first frame update
    public enum enemyStates
    {
        Chase,
        Patrol
    }

    [SerializeField] enemyStates currentState;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (body == null)
        {
            Destroy(this);
        }

        switch (currentState)
        {
            case enemyStates.Patrol:
                if (isColliding == false)
                {
                    randomMovement();
                }

                break;
            case enemyStates.Chase:

                chasing(unit);
                break;
            default:
                currentState = enemyStates.Patrol;
                break;
        }


        // clamp the trasform position of the enemy 
        // you can use Mathf.clamp for the bounding part of the world


        float x = Mathf.Clamp(transform.position.x, -30, 30);
        float y = Mathf.Clamp(transform.position.y, -30, 30);

        this.transform.position = new Vector3(x, y, this.transform.position.z);

    }

    void PatrolUpdate()
    {
        Debug.Log("Patrol Updating");
    }
    void ChaseUpdate()
    {
        Debug.Log("Chase Updating");
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        Debug.Log(other.gameObject.name);
        unit = other.gameObject;

        if (other.gameObject.tag == "Player")
        {

                currentState = enemyStates.Chase;

        }
        else if (other.gameObject.tag == "Enemy")
        {

                currentState = enemyStates.Chase;
        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        Debug.Log(other.gameObject.name);
        safe = true;
        chase = false;
        isColliding = false;
        currentState = enemyStates.Patrol;
    }

    void randomMovement()
    {
        if (curLocationTime <= 0)
        {

            movement = new Vector3(Random.Range(-30, 30), Random.Range(-30, 30), 0);
            curLocationTime = locationTime + this.transform.localScale.x;
        }
        else
        {
            if (isColliding == false)
            {
                transform.position = Vector2.MoveTowards(transform.position, movement, (moveSpd / body.transform.localScale.x) * Time.deltaTime);
            }
            curLocationTime -= 1 * Time.deltaTime;

        }

    }
    void chasing(GameObject obj)
    {
        chase = true;
        isColliding = true;
        if (chase == true)
        {
            if (obj.tag == "Player")
            {

                transform.position = Vector2.MoveTowards(transform.position, obj.transform.position, (moveSpd / body.transform.localScale.x) * Time.deltaTime);
            }
            else if (obj.tag == "Enemy")
            {

                transform.position = Vector2.MoveTowards(transform.position, obj.transform.position, (moveSpd / body.transform.localScale.x) * Time.deltaTime);
            }
        }

    }

}
