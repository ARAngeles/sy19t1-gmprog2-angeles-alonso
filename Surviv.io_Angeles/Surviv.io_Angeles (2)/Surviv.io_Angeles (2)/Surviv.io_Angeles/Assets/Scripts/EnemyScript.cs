﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    Rigidbody2D rb;
    SpriteRenderer enemySprite;
    public GameObject obj;
    public CircleCollider2D detector;

    public Color[] colorarray;
    // Start is called before the first frame update
    void Start()
    {
        enemySprite = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        int colorNumber = Random.Range(0, 5);
        enemySprite.color = colorarray[colorNumber];
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = obj.transform.position;

    }
}
