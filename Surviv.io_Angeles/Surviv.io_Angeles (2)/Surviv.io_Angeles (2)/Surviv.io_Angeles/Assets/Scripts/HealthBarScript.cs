﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarScript : MonoBehaviour
{
    public Transform bar;
    public GameObject barfill;
    public GameObject player;
    private Vector3 offset;
    // Start is called before the first frame update

    private void Start()
    {
        offset = new Vector3(3.07f, -4.5f, 0);
    }
    public void SetSize(float sizeNormalized)
    {
        bar.localScale = new Vector3(sizeNormalized, 1f);
    }
    public void SetColor(Color color)
    {
        barfill.GetComponent<SpriteRenderer>().color = color;
    }
    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(player.transform.position.x + offset.x, player.transform.position.y + offset.y, offset.z);
    }
}
