﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponScript : MonoBehaviour
{
    public Transform barrel;

    public GameObject bullet;
    PlayerScript player;
    public int capacity;
    public int ammo;

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 dir = Input.mousePosition - mousePos;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);


        if(ammo == 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                
            }
        }
        else if (Input.GetMouseButtonDown(0))
        {
            FireBullet();
        }

        if(Input.GetKeyDown(KeyCode.R))
        {
            
        }
    }

    public virtual void FireBullet()
    {
        GameObject firedBullet = Instantiate(bullet, barrel.position, barrel.rotation);
        capacity--;
        firedBullet.GetComponent<Rigidbody2D>().velocity = barrel.up * 30f;

    }

}
