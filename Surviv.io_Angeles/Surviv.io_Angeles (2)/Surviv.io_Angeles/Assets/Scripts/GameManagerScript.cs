﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScript : MonoBehaviour
{
    [SerializeField] private PlayerScript player;
    public Transform minSpawnPoint;
    public Transform maxSpawnPoint;
    public int maxSpawned;
    int objCount = 0;
    public GameObject ammo;
    public GameObject guns;
    public GameObject objects;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnObjects()
    {
        float x = Random.Range(minSpawnPoint.position.x, maxSpawnPoint.position.x);
        float y = Random.Range(minSpawnPoint.position.y, maxSpawnPoint.position.y);
        Vector3 spwnPos = new Vector3(x, y, 0);
        var newObjs = GameObject.Instantiate(objects, spwnPos, Quaternion.identity);
    }
    public void RemoveObjects(GameObject objects)
    {
        Destroy(objects.gameObject);
    }
    public void RemoveCollider(CircleCollider2D collider)
    {
        Destroy(collider);
    }
    void addAmmo(int amount)
    {
        player
    }
}
