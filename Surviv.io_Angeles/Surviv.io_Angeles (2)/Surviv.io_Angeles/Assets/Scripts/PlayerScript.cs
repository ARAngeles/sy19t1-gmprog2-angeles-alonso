﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{

    [SerializeField] private HealthBarScript healthBar;

    float moveSpd = 10f;
    public Rigidbody2D rb;
    SpriteRenderer playerSprite;
    Vector2 movement;
    bool hasWeapon = false;
    public Color[] colorarray;
    float hp = 1f;
    int pistolAmmo = 0;
    int rifleAmmo = 0;
    int shotgunAmmo = 0;

    // Start is called before the first frame update
    void Start()
    {
        playerSprite = GetComponent<SpriteRenderer>();
        int colorNumber = Random.Range(0, 5);
        playerSprite.color = colorarray[colorNumber];
    }

    // Update is called once per frame
    void Update()
    {
        //virtual bool keyPressed();
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    rb.AddForce(rb.position + movement * moveSpd * Time.fixedDeltaTime);
        //}

        //if (Input.GetKeyDown(KeyCode.D))
        //{
        //    rb.AddForce(rb.position + movement * moveSpd * Time.fixedDeltaTime);
        //}
        //if (Input.GetKeyDown(KeyCode.W))
        //{
        //    rb.AddForce(rb.position + movement * moveSpd * Time.fixedDeltaTime);
        //}
        //if (Input.GetKeyDown(KeyCode.S))
        //{
        //    rb.AddForce(rb.position + movement * moveSpd * Time.fixedDeltaTime);
        //}



        Vector3 mousePos = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 dir = Input.mousePosition - mousePos;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        

        if(hp <= .5f)
        {
            healthBar.SetColor(Color.yellow);
        }
        else if(hp <= .25)
        {
            healthBar.SetColor(Color.red);
        }
        else if (hp > .5f)
        {
            healthBar.SetColor(Color.white);
        }

        //Vector3 direction = new Vector3(0, 0, dir.z);
        //transform.LookAt(dir, Vector3.forward);
    }
    void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * moveSpd * Time.fixedDeltaTime);

        if (Input.GetKey(KeyCode.KeypadPlus))
        {
            hp += 0.01f;
            healthBar.SetSize(hp);
        }
        if (Input.GetKey(KeyCode.KeypadMinus))
        {
            hp -= 0.01f;
            healthBar.SetSize(hp);
        }

    }

    public void pickUpAmmo(int type, int amount)
    {
        if(type == 1)
        {
            pistolAmmo += amount;
        }
        else if(type == 2)
        {
            rifleAmmo += amount;
        }
        else
        {
            shotgunAmmo += amount;
        }
    }

    public void Reload(int capacity,int type)
    {
        
    }

}
